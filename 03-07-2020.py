import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from util import GrowthRateAverage, linearRegression

# Transforms absolute values into percentage.


def axe_formating(axe):
    return [round(i, 1) for i in axe.get_yticks()*100]


df_brazil = pd.read_excel(r'./data/03-07-2020-brazil.xlsx')

# Dataset info.
# print(df_brazil.describe())

# Average value for growth rate.
r = GrowthRateAverage(df_brazil)

# ------------------- Chart 1 Exponencial relationship -------------------

# Exponential function ranges.
df_brazil['exp_1000'] = np.exp(np.linspace(0, 7, df_brazil['date'].size))
df_brazil['exp_10000'] = np.exp(np.linspace(0, 10, df_brazil['date'].size))
df_brazil['exp_100000'] = np.exp(np.linspace(0, 11, df_brazil['date'].size))
df_brazil['exp_1000000'] = np.exp(np.linspace(0, 14, df_brazil['date'].size))
df_brazil['exp_10000000'] = np.exp(np.linspace(0, 16, df_brazil['date'].size))
# Total cases
fig_one, axes_one = plt.subplots(nrows=2, ncols=2, sharex=False)
df_brazil.iloc[:, [0, 2, 5]].plot(
    ax=axes_one[0][0], title='Deaths vs $e^x$',  color=['red', 'green'], x='date')
df_brazil.iloc[:, [0, 1, 3]].plot(
    ax=axes_one[1][0], title='Total cases vs Total Recovered',  color=['orange', 'green'], x='date')
df_brazil.iloc[:, [0, 1, 6]].plot(
    ax=axes_one[0][1], title='Total cases vs $e^x$',  color=['orange', 'green'], x='date')
df_brazil.iloc[:, [1, 2]].plot(
    ax=axes_one[1][1], title='Total cases vs Total deaths',  color=['orange', 'green'], x='total_cases')

axes_one[1][1].plot(df_brazil.iloc[:, 1],

                    linearRegression(df_brazil.iloc[:, 1].values.reshape(-1, 1),
                                     df_brazil.iloc[:, 2].values.reshape(-1, 1))

                    )


# Chart styling
fig_one.suptitle("COVID-19 Brazi's contex in July 3, 2020")
axes_one[0][0].set_ylabel('Deaths')
axes_one[0][0].set_xlabel('Date')
axes_one[0][0].legend(['Deaths', '$e^x$'])
axes_one[0][1].set_ylabel('Cases')
axes_one[0][1].set_xlabel('Date')
axes_one[0][1].legend(['Cases', '$e^x$'])

axes_one[1][0].set_ylabel('Cases')
axes_one[1][0].set_xlabel('Date')
axes_one[1][0].legend(['Cases', 'Recovered'])

axes_one[1][1].legend(['Cases x Deaths', 'Model'])
axes_one[1][1].set_ylabel('Death toll')
axes_one[1][1].set_xlabel('Total cases')

# # ------------------- Chart 2 Total cases -------------------
# fig_two, axes_two = plt.subplots()
# total_cases_chart = df_brazil[(df_brazil.iloc[:, [0, 1]]['total_cases']) > 0].plot.bar(
#     ax=axes_two, title='Total de casos confirmados até 12/04/2020',  color=['orange'], x='date', y='total_cases')


# # Chart styling
# axes_two.legend(['casos confirmados'], loc="upper left")
# axes_two.set_xlabel('Dia')
# axes_two.set_ylabel('Casos')
# # Numbers on the top
# for p in total_cases_chart.patches:
#     total_cases_chart.annotate(str(p.get_height()), (p.get_x()
#                                                      * 1.005, p.get_height() * 1.005))

# # ------------------- Chart 3 logistic map simulations -------------------
# fig_four, axes_four = plt.subplots()

# cenarios = [[1e-6], [1e-6], [1e-6], [1e-6], [1e-6], [1e-6]]
# for i in range(1, 200):
#     cenarios[0].append(r*cenarios[0][-1]*(1-cenarios[0][-1]))
#     # r = 1.20
#     cenarios[1].append(1.20*cenarios[1][-1]*(1-cenarios[1][-1]))
#     # r = 1.10
#     cenarios[2].append(1.10*cenarios[2][-1]*(1-cenarios[2][-1]))
#     # r = 1.15
#     cenarios[3].append(1.15*cenarios[3][-1]*(1-cenarios[3][-1]))
#     # r = 1.09
#     cenarios[4].append(1.09*cenarios[4][-1]*(1-cenarios[4][-1]))
#     # r = 1.60 a darkness scenario
#     cenarios[5].append(1.80*cenarios[5][-1]*(1-cenarios[5][-1]))

# axes_four.plot(cenarios[0], '-k', color='purple')
# axes_four.plot(cenarios[1], '-k', color='salmon')
# axes_four.plot(cenarios[2], '-k', color='steelblue')
# axes_four.plot(cenarios[3], '-k', color='darkorange')
# axes_four.plot(cenarios[4], '-k', color='yellowgreen')
# axes_four.plot(cenarios[5], '-k', color='red')

# # Chart styling
# axes_four.grid(alpha=0.5)
# fig_four.suptitle(
#     r'Modelagem logistítca Brasil até 05/07/2020 $n_{i+1}$ = r * $n_i$ * (1 - $\frac{n_i}{N}$)', fontsize=18)
# # Axes formating
# axes_four.legend([('r = %s (situação real em 05/07/2020)') % round(r, 2), 'r = 1.20',
#                   'r = 1.15', 'r = 1.10', 'r = 1.09', 'r = 1.80 (afrouxamento do isolamento)'], loc="upper right")
# axes_four.set_ylabel('% População afetada')
# axes_four.set_xlabel('Quantidade de dias')
# axes_four.set_yticklabels(axe_formating(axes_four))

plt.show()
