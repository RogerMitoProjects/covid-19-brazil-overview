<link rel="stylesheet" type="text/css" href="./main.css">

# A simplified overview of COVID-19's effects in Brazil

This script performs some statical analysis to help us understand how covid's spread affects Brazillian people.
Pandas lib is used with the logistic map to facilitate data transforming, analyzing and plotting.

All data were got from World Health Organization and it's possible to check it out here: [Brazil official data](https://www.worldometers.info/coronavirus/country/brazil/)

## Disclaimer

**_All forecasts on this project are just approximations. The expected results may change according to people's behavior and public politics adopt during the overbreak._**

In this context, it's important to remind the quote of famous University of Wisconsin — Madison statistician [Prof George Box](https://en.wikipedia.org/wiki/George_E._P._Box):

> [...] All models are approximations. Essentially, all models are wrong, but some are useful. However, the approximate nature of the model must always be borne in mind [...]

## About logistic map equation

There are a lot of diferent places witch help us to understand how logistic map works. [Here](https://www.youtube.com/watch?v=ovJcsL7vyrk&feature=youtu.be) you can watch a great video about it.

The simple equation is of this form:

<img src="./eq.png" width="300" style="margin-left:25%">

Where, n<sub>i+1</sub> is the nth generation of value n and n is real number between [0, 1], r is the growth rate which is a real number > 0 and N is the total number of samples.

[Chaos theory](https://geoffboeing.com/2015/03/chaos-theory-logistic-map/) is a field of mathematics dealing with nonlinear dynamical systems that may be explained by logistic map behavior. Besides, this equation can be used to model outbreaks diseases like COVID-19.

## Stay home

The good news is, just as you can easily transmit the virus to other people, you can easily avoid transmitting it — if you’re willing to stay home. That’s right: Simply by sitting on your couch, you can potentially save lives.

<img src="./transmission_graphic.gif">

<img src="./dot.png" width="25px"> Infected person

## Some charts from July 3, 2020

<img src="./brazil-07-03-2020.png">
