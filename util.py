
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

# Defines the average value for growth rate (r).


def GrowthRateAverage(totalCases):
    r = 0
    for index in range(0, (totalCases['total_cases'].size - 1)):
        if totalCases['total_cases'][index] > 0 and totalCases['total_cases'][index+1] > 0:
            r += totalCases['total_cases'][index+1] / \
                totalCases['total_cases'][index]

    r /= totalCases[(totalCases['total_cases']) > 0]['total_cases'].count()
    return r

# Defines the linear regression between total cases and death toll.


def linearRegression(totalCases, deathToll):
    # Splits 80% of the data to training set while 20% of the data to test set.
    linearRegression = LinearRegression()
    # xTrain, xTest, yTrain, yTest = train_test_split(
    #     totalCases, deathToll, test_size=0.3, random_state=0)
    # linearRegression.fit(xTrain, yTrain)
    linearRegression.fit(totalCases, deathToll)
    print('\n', '\n')
    print('Linear function: \n', ('y = %sx + %s') %
          (linearRegression.coef_[0][0], linearRegression.intercept_[0]))
    print('\n', '\n')
    predicted = linearRegression.predict(totalCases)

    # yPred = linearRegression.predict(xTest)
    df = pd.DataFrame(
        {'Actual': deathToll[:, 0], 'Predicted': predicted[:, 0]})
    print(df.to_markdown())

    # print(linearRegression.coef_, linearRegression.intercept_)
    return predicted
